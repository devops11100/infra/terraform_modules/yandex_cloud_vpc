output "subnets" {
  value = {
    for k in yandex_vpc_subnet.vpc-subnet : k.zone => k.id...
  }
}

output "addr" {
  value = {
    for k in yandex_vpc_address.vpn-addr : k.name => k.external_ipv4_address[0].address
  }
}

output "network" {
  value = yandex_vpc_network.vpc-network.id
}