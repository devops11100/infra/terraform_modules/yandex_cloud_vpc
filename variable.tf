variable "vpc-network-name" {
  type        = string
  description = "VPC Network name"
}

variable "vpc-subnet-name" {
  type        = string
  description = "VPC Subnet name"
}

variable "vpc-network-description" {
  type        = string
  description = "VPC Network description"
}

variable "vpc-subnet-cidr" {
  type        = list(any)
  description = "VPC Subnet cird"
}

variable "yandex-zones" {
  type        = list(any)
  description = "Yandex Cloud zones"
  default     = ["ru-central1-a", "ru-central1-b", "ru-central1-c"]
}

variable "vpc-rout-table-name" {
  type        = string
  description = "VPC Routtable name"
}

variable "vpc-address-name" {
  type        = list(any)
  description = "VPC Address name"
}