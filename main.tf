resource "yandex_vpc_network" "vpc-network" {
  name        = var.vpc-network-name
  description = var.vpc-network-description

}

resource "yandex_vpc_subnet" "vpc-subnet" {
  count          = length(var.vpc-subnet-cidr)
  name           = "${var.vpc-subnet-name}-${count.index}"
  v4_cidr_blocks = var.vpc-subnet-cidr[count.index]
  zone           = var.yandex-zones[count.index <= 2 ? count.index : count.index % 3]
  network_id     = yandex_vpc_network.vpc-network.id
  route_table_id = yandex_vpc_route_table.rout-table.id
}

resource "yandex_vpc_gateway" "nat-gateway" {
  name = "prod-nat-gateway"
  shared_egress_gateway {}
}

resource "yandex_vpc_route_table" "rout-table" {
  name       = var.vpc-rout-table-name
  network_id = yandex_vpc_network.vpc-network.id

  static_route {
    destination_prefix = "0.0.0.0/0"
    gateway_id         = yandex_vpc_gateway.nat-gateway.id
  }
}

resource "yandex_vpc_address" "vpn-addr" {
  count = length(var.vpc-address-name)
  name  = var.vpc-address-name[count.index]

  external_ipv4_address {
    zone_id                  = "ru-central1-a"
    ddos_protection_provider = "qrator"
  }
}

